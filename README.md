# Affari-Microsite with SASS-webpack4
## Microsite for \client/

* webpack 4 with loaders and plugins
* bootstrap 4 + popper.js with sass compiler
* font-awesome free
* jQuery + jQuery easing
* Affari scrolling bar and stick nav JS
* GreenSock 'gsap' JS
* animeJS
* flickity carousel
* WOW JS
* animate.css
* modernizr
* Babel ES6 Compiler
* autoprefixer
* create gzip (.gz) css and js = increase performance on Browser


### start
go to your new project folder: open terminal and type:

$ git clone https://bitbucket.org/affarimedia/affari-boilerplate-html-sass.git

- delete folder .git;
- install npm:
```
npm i
```

### commands:
1)
start developer server and watch (http://localhost:8080)
```
npm start
```
2)
production mode
```
npm run build
```


### html and sass
Add your HTML files by inserting or including them in the 'src' directory.
Make sure you restart development server after adding new HTML files
```
npm start
```
Add images, docs, videos: 'src/media' folder.

Add sass (.scss) files to 'src/sass', named then like '_custom.scss' and import then in 'main.scss'
```
@import "sassfilename";
```

### additional libraries

go to 'src/index.js' and uncomment the line for enable



----------------------------------------
Licenses & Credits
----------------------------------------
- WebPack: https://webpack.js.org (MIT)
- GZIP Compression: https://www.gnu.org/software/gzip (GNU)
- Babel ES 6: https://babeljs.io (MIT)
- Bootstrap: http://getbootstrap.com (MIT)
- jQuery: https://jquery.org | (Code licensed under MIT)
- Font Awesome: http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
- Flickity: https://flickity.metafizzy.co/license.html (GPLv3)
- GreenSock JS: https://greensock.com/standard-license
- animejs: https://animejs.com/ (MIT)
- WOW JS: https://wowjs.uk (MIT)
- Animate.css: https://github.com/daneden/animate.css/blob/master/LICENSE (MIT)
- jQuery RWD Image Maps: https://github.com/stowball/jQuery-rwdImageMaps (MIT)
- Modernizr: https://modernizr.com/ (Modernizr 3.5.0 | MIT)
